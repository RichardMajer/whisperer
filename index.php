<?php include_once "parts/header.php"; ?>

	<main class="body__main " id="body__main">
        <div class="container" id="main__container">
            <div class="whisperer" id="whisperer">
                <h2 class="whisperer__headline">Vyhľadať v databáze obcí</h2>
                <form action="detail.php" class="whisperer__form col-12 col-md-8 col-lg-6" id="whisperer__form">
                    <input type="text" name="search" id="search" placeholder="Zadajte názov" class="form-control whisperer__form__input col-12" />
                    <ul class="  p-0 whisperer__form__recommendation" id="result"></ul>
                </form>
            </div>
        </div>
	</main>

<?php include_once "parts/footer.php"; ?>


