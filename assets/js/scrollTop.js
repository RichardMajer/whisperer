$(document).ready(function(){
    // ANIMATE SCROLL TOP
    var scrollToTop = $('#scrollToTop'),
        win     = $(window),
        page    = $('body, html');

    scrollToTop
        .hide()
        .on('click' , function(event){
            event.preventDefault();
            page.animate({scrollTop: 0}, 500 , 'easeInOutQuint')
        });

    win.on('scroll', function(){
        if (win.scrollTop() >= 200 ) scrollToTop.fadeIn();
        else scrollToTop.fadeOut();
    });

});