//=============================================
// EXTERNAL SOURCE OF DATABSE
// https://api.myjson.com/bins/sqqw0
//=============================================
$(document).ready(function(){

    // INPUT VARIABLES
    var search = $('#search'),
        result = $('#result');

    // AUTO FOCUS ON SEARCH INPUT AND CLEAR HIM VALUE
    search.focus().val('');
    $.ajaxSetup({ cache: false });

    // AFTER CLICK ANYWHERE ON PAGE BODY, SLIDEUP SEARCH WHISPERER
    $('body').on('click', function () {
        result.slideUp();
    });

    // ===============================================
    // FUNCTION INVOKIN AFRTER CLICK OR PRESS ANY KEY IN INPUT
    // ===============================================
    search.on('click keyup' , function(){

        //RESET VALUES OF RESULT
        result.html('');

        // SHOW RESULT
        result.show();

        // VARIALBLES NEEDED TO GET DATA FROM DATABASE
        var searchField = search.val(),
             expression = new RegExp(searchField, "i");

        // LOADING DATA FROM JSON DATABASE BY AJAX
        $.getJSON('database.json', function(data) {

                // LOOPS FOR LOADING EVERY DATA FROM DATABASE
                for(var i= 0; i<(data).length; i++) {
                    // CONDITIONS FOR COMPARE CHARS IN INPUT AND DATABASE
                    if (
                      data[i].nazov_obce.search(expression) != -1 ||
                      data[i].meno_starostu.search(expression) != -1
                    )
                    // SHOW EVERY CONSISTENT DATA IN LIST ITEM FORM
                    { result
                        .append('<li id="'+ data[i].id +'" class="whisperer__form__recommendation__item">' +
                                    '<img  alt="coat of arms" src="'+ data[i].erb+'" ' +
                                            'class="whisperer__form__recommendation__item__img" ' +
                                            'title="coat of arms"/> ' +
                            ''+ data[i].nazov_obce+' | '+data[i].meno_starostu+'</li>');
                    }
                }

           // ===============================================
           // FUNCTION INVOKIN AFRTER CLICK ON CHOSEN ITEM
           // ===============================================
           result.on('click', 'li', function() {

                // FORM FARIABLES
                var     form = $('#whisperer__form'),
                   formInput = $('.whisperer__form__input');

                // AFTER CLICK ON CHOSEN ITEM SET INPUT VAL TO ID OF THIS ITEM
                formInput.val($(this).attr("id"));

                // AFTER CLICK ON CHOSEN ITEM SUBMIT FORM
                form.submit();

            });
        });
    });
});