$(document).ready(function(){

    // LOADING DATA FROM JSON DATABASE BY AJAX
    $.getJSON('database.json', function(data) {

        // VARIABLES FOR DETAIL TABLE OF CITY
        var queryStringVal = location.search.split("=").pop(),
                chosenCity = (data[queryStringVal - 1]),
              nameOfCity   = $('#right-card-text'),
              coarOfArms   = $('#right-card-img'),
              nameOfMayor  = $('#name'),
              officeAdress = $('#adress'),
                  telNum   = $('#tel'),
                  faxNum   = $('#fax'),
              mailAdress   = $('#mail'),
                  webPage  = $('#web'),
               splitwebUrl = chosenCity.web.split('https://'),
             coordinations = $('#coordinates');

        // FILLING THE TABLE
        nameOfCity.text( chosenCity.nazov_obce);

        coarOfArms.attr('src',chosenCity.erb)
            .attr('alt','coat of arms')
            .attr('title','coat of arms');

        nameOfMayor.text(chosenCity.meno_starostu);

        officeAdress.text(chosenCity.adresa_uradu);

        telNum.attr('href','tel:'+chosenCity.tel)
            .attr('title','tel. nubmer')
            .text(chosenCity.tel);

        mailAdress.attr('href','mailto:'+chosenCity.email)
            .attr('title','mail adress')
            .text(chosenCity.email);

        faxNum.attr('href','fax:'+chosenCity.fax)
            .attr('title','fax adress')
            .text(chosenCity.fax);

        webPage.attr('href',chosenCity.web)
            .attr('title','web page')
            .attr('target','_blunk')
            .text(splitwebUrl[1]);

        coordinations.text(chosenCity.suradnice);

    })
});
