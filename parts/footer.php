<footer class="body__footer">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-6 col-lg-3">
                <p class="group-list__headline">Adresa a kontakt</p>
                <ul class="group-list">
                    <li class="group-list__item--muted group-list__item--style">ŠÚKL</li>
                    <li class="group-list__item--muted group-list__item--style">Kvetná 11</li>
                    <li class="group-list__item--muted group-list__item--style">825 08 Bratislava 26</li>
                    <li class="group-list__item--muted group-list__item--style">Ústredňa</li>
                    <li class="group-list__item--muted group-list__item--style">+421-2-50701 111</li>
                </ul>

                <p class="group-list__headline">Kontakty</p>
                <ul class="group-list">
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">telefónne čísla</a>
                    </li>
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">adresa</a>
                    </li>
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">úradné hodiny</a>
                    </li>
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">bankové spustenie</a>
                    </li>
                </ul>

                <p class="group-list__headline">Informácie pre verejnosť</p>
                <ul class="group-list">
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">Zoznam vyvezených liekov</a>
                    </li>
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">MZ SR</a>
                    </li>
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">Národný portál zdravia</a>
                    </li>
                </ul>
            </div>

            <div class="col-12 col-sm-6 col-lg-3 p-lg-0">
                <p class="group-list__headline">O nás</p>
                <ul class="group-list">
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">Dotazníky</a>
                    </li>
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">Hlavní predstavitelia</a>
                    </li>
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">Základné dokumenty</a>
                    </li>
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">Zmluvy za ŠÚKL</a>
                    </li>
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">História a súčasnosť</a>
                    </li>
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">Národná spolupráca</a>
                    </li>
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">Medzinárodná spolupráca</a>
                    </li>
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">Poradné orgány</a>
                    </li>
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">Legislatíva</a>
                    </li>
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">Priestupky a iné správne delikty</a>
                    </li>
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">Zoznam dlžníkov</a>
                    </li>
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">Sadzobník ŠÚKL</a>
                    </li>
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">Verejné obstarávanie</a>
                    </li>
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">Vzdelávacie akcie a prezentácie</a>
                    </li>
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">Konzultácie</a>
                    </li>
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">Voľné pracovné miesta (0)</a>
                    </li>
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">Poskytovanie informácií</a>
                    </li>
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">Sťažnosti a petície</a>
                    </li>
                </ul>
            </div>

            <div class="col-12 col-sm-6 col-lg-3">
                <p class="group-list__headline">Médiá</p>
                <ul class="group-list">
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">Tlačové správy</a>
                    </li>
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">Lieky v médiách</a>
                    </li>
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">Kontakt pre médiá</a>
                    </li>
                </ul>

                <p class="group-list__headline">Databáza a servis</p>
                <ul class="group-list">
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">Databáza liekov a zdravotníckych pomôcok</a>
                    </li>
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">Iné záznamy</a>
                    </li>
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">Kontaktný formulár</a>
                    </li>
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">Mapa stránok</a>
                    </li>
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">A - Z index</a>
                    </li>
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">Linky</a>
                    </li>
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">RSS</a>
                    </li>
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">Doplnok pre internetový prehliadač</a>
                    </li>
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">Prehliadače formátov</a>
                    </li>
                </ul>
            </div>

            <div class="col-12 col-sm-6 col-lg-3 group-list--padding">
                <p class="group-list__headline">Drogové prekurzory</p>
                <ul class="group-list">
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">Aktuality</a>
                    </li>
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">Legislatíva</a>
                    </li>
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">Pokyny</a>
                    </li>
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">Kontakt</a>
                    </li>
                </ul>

                <p class="group-list__headline">Iné</p>
                <ul class="group-list">
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">Linky</a>
                    </li>
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">Mapa stránok</a>
                    </li>
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">FAQ</a>
                    </li>
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link">Podmienky používania</a>
                    </li>
                </ul>

                <p class="group-list__headline group-list--decoration">Rapid alert system</p>
                <ul class="group-list">
                    <li class="group-list__item--muted">
                        <a href="#" title="link" class="group-list__item__link group-list__item--decoration">Rýchla výstraha vyvplývajúca z nedostatkov v kvalite liekov</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="scroll-to-top">
        <a href="#" id="scrollToTop" class="scroll-to-top__icon" title="scroll button to top">
            <i class="fa fa-caret-up fa-5x"></i>
        </a>
    </div>
</footer>

<!-- Jquery -->
<script src="assets/node_modules/jquery/dist/jquery.min.js"></script>
<!-- Animation scroll top -->
<script src="assets/bower_components/animatescroll/animatescroll.min.js"></script>
<script src="assets/js/scrollTop.js"></script>
<!-- Whisperer function -->
<script src="assets/js/whisperer.js"></script>
<script src="assets/js/whispererOutput.js"></script>

<!--SLIDE MENU-->
<!-- TouchSwipe library -->
<script src="http://labs.rampinteractive.co.uk/touchSwipe/jquery.touchSwipe.min.js"></script>
<!-- Sliding swipe menu javascript file -->
<script src="assets/node_modules/slide-and-swipe-menu/jquery.slideandswipe.js"></script>
<!-- My own JS file for slide menu -->
<script src="assets/js/slideMenu.js"></script>

</body>
</html>
