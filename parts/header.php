<!doctype html>
<html lang="sk">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="Lorem ipsum dolor sit amet, mei platonem theophrastus ne, dicat nostro cu mei. Sit ea altera eruditi ." />
    <meta name="keywords" content="EFFECTIVE CLEANING &amp; GARDENING" />
    <meta name="author" content="Richard Majer" />
    <meta name="robots" content="index" />

    <meta property="og:title" content="EFFECTIVE CLEANING &amp; GARDENING" />
    <meta property="og:description" content="Lorem ipsum dolor sit amet, mei platonem theophrastus ne, dicat nostro cu mei. Sit ea altera eruditi." />
    <meta property="og:site_name" content="EFFECTIVE CLEANING &amp; GARDENING" />
    <meta property="og:url" content="" />

    <title>EFFECTIVE CLEANING &amp; GARDENING</title>

    <link rel="shortcut icon" type="image/png" href="assets/img/favicon.png">
    <link rel="stylesheet" href="assets/node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

    <!--UNIQUE NAME FOR STYLESHEET FILE IN HIS EVERY MODIFICATION-->
    <?php
    $stylesheetName     = 'assets/css/style.css';
    $stylesheetModified = substr(md5(filemtime($stylesheetName)), 0, 6);
    ?>

    <link rel="stylesheet" type="text/css" href="<?php echo $stylesheetName;?>?v=<?php echo $stylesheetModified ; ?>">

</head>

<body class="body">

    <header class="body__header">
        <div class="container header-container">
            <div class="logo">
                <a href="#" title="this company">
                    <img src="assets/img/logo.png" alt="logo of company" class="img-fluid" title="logo">
                </a>
            </div>

            <nav class="slide-menu">
                <form action="#" method="post" class="form">
                    <a href="#" class="form__contacts" title="contacts">Kontakty a čisla na oddelenia</a>
                    <select id="language" name="country" class="form__selector">
                        <option value="EN">EN</option>
                        <option value="SK">SK</option>
                        <option value="CZ">CZ</option>
                        <option value="DE">DE</option>
                    </select>
                    <div class="form__search ">
                        <span class="fa fa-search form__search__icon"></span>
                        <input type="text" class=" form__search__input form-control form-control-sm" placeholder="">
                    </div>
                    <button class="form__button form__button--success form__button--small">Prihlásenie</button>
                </form>

                <nav class="col-12 navigation">
                    <ul class="navigation__group" id="navigation__group">
                        <li class="navigation__group__item navigation__group__item--first-item">
                            <a href="#" title="O nas" class="navigation__group__item__link">O nás</a>
                        </li>
                        <li class="navigation__group__item">
                            <a href="#" title="Zoznam miest" class="navigation__group__item__link">Zoznam miest</a>
                        </li>
                        <li class="navigation__group__item">
                            <a href="#" title="Inspekcia" class="navigation__group__item__link">Inšpekcia</a>
                        </li>
                        <li class="navigation__group__item navigation__group__item--last-item">
                            <a href="#" title="Kontakt" class="navigation__group__item__link">Kontakt</a>
                        </li>
                    </ul>
                </nav>
            </nav>

            <div class="navigation__icon text-right" id="navigation__icon">
                <img src="assets/img/64x64.png" alt="menu icon" class="ssm-toggle-nav navigation__icon__img">
            </div>
            <div class="slide-menu__overlay ssm-toggle-nav"></div>
        </div>
    </header>