<?php include_once "parts/header.php"; ?>

	<main class="body__main body__main--detail" id="body__main ">
        <div class="container" id="main__container">
            <div id="output" class="whisperer__form__output col-12">
                <h2 class="whisperer__form__output__headline">Detail obce</h2>
                <div class="whisperer__form__output__card row">
                    <div class="whisperer__form__output__card__left order-2 order-md-1 d-flex justify-content-center align-items-center col-12 col-md-6">
                        <table class="whisperer__form__output__card__left__table col-12 col-md-10 ">
                            <tbody>
                            <tr>
                                <th scope="row">Meno starostu:</th>
                                <td id="name"></td>
                            </tr>
                            <tr>
                                <th scope="row">Adresa obecného úradu:</th>
                                <td id="adress"></td>
                            </tr>
                            <tr>
                                <th scope="row">Telefón:</th>
                                <td><a href="" id="tel"></a></td>
                            </tr>
                            <tr>
                                <th scope="row">Fax:</th>
                                <td><a href="" id="fax"></a></td>
                            </tr>
                            <tr>
                                <th scope="row">Email:</th>
                                <td><a href="" id="mail"></a></td>
                            </tr>
                            <tr>
                                <th scope="row">Web:</th>
                                <td><a href="" id="web"></a></td>
                            </tr>
                            <tr>
                                <th scope="row">Zemepisné súradnice:</th>
                                <td id="coordinates"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <figure class="whisperer__form__output__card__right order-1 order-md-2 col-12 col-md-6 d-flex flex-column justify-content-center align-items-center">
                        <img src="assets/img/logo.png" alt="" id="right-card-img" class="whisperer__form__output__card__right__img img-fluid">
                        <figcaption class="whisperer__form__output__card__right__text" id="right-card-text"></figcaption>
                    </figure>
                </div>
            </div>
        </div>
	</main>

<?php include_once "parts/footer.php"; ?>


